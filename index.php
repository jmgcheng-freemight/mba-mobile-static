<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="UTF-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0" />
		
		<link rel="stylesheet" type="text/css" href="css/reset.css" />
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="js/slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="js/slick/slick-theme.css"/>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/media.css" />
		
		<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="js/slick/slick.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		
		<title>MBA</title>
	</head>
	<body>
		<header id="site-header">
			<h1 class="site-banner">
				<a class="site-logo" href="index.php">
					MBA
				</a>
			</h1>
			<nav class="site-menu site-header-menu main-site-header-menu">
				<ul>
					<li class="site-header-menu-items global">
						<a href="#" class="anc anc-list-opener ">
							<span class="glyph glyph-menu-open-white"></span><span class="text">MENU</span>
						</a>
						<ul class="site-submenu site-header-submenu main-site-header-submenu lvl2 display-none">
							<li>
								<a href="#">
									学校案内
								</a>
							</li>
							<li>
								<a href="#" class=" anc-list-opener ">
									コース紹介 / 料金
								</a>
								<ul class="site-submenu site-header-submenu main-site-header-submenu lvl3 display-none">
									<li>
										<a href="#">
											MBA GOLD
										</a>
									</li>
									<li>
										<a href="#">
											MBA SILVER
										</a>
									</li>
									<li>
										<a href="#">
											割引プラン
										</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="#">
									1日のスケジュール
								</a>
							</li>
							<li>
								<a href="#">
									卒業生の声 〜Cross Story〜
								</a>
							</li>
							<li>
								<a href="#">
									ご入学までの流れ
								</a>
							</li>
							<li>
								<a href="#">
									よくあるご質問
								</a>
							</li>
							<li>
								<nav class="social-site-header-menu">
									<ul class="site-submenu site-header-submenu main-site-header-submenu lvl3 ">
										<li>
											<a href="#">
												<img src="images/glyph-facebook-square-gray.png" />
												<!-- 
												<span class="glyph glyph-facebook-square-gray"></span>
												-->
											</a>
										</li>
										<li>
											<a href="#">
												<img src="images/glyph-twitter-square-gray.png" />
												<!-- 
												<span class="glyph glyph-twitter-square-gray"></span>
												-->
											</a>
										</li>
										<li>
											<a href="#">
												<img src="images/glyph-instagram-square-gray.png" />
												<!-- 
												<span class="glyph glyph-instagram-square-gray"></span>
												-->
											</a>
										</li>
										<li>
											<a href="#">
												<img src="images/glyph-line-square-gray.png" />
												<!-- 
												<span class="glyph glyph-line-square-gray"></span>
												-->
											</a>
										</li>
										<div class="clear-both"></div>
									</ul>
								</nav>
							</li>
						</ul>
					</li>
					<li class="site-header-menu-items contact">
						<a href="#" class="anc anc-list-opener">
							<span class="glyph glyph-contacts-white"></span><br/>
							<span class="text">CONTACT</span>
						</a>
						<ul class="site-submenu site-header-submenu main-site-header-submenu lvl2 display-none">
							<li>
								<div class="inquiry-blocks inquiry-block-2">
									<div class="inquiry-block-action-col1">
										<p>
											<span class="glyph glyph-phone-blue"></span>03-4405-8478<br/>
											<span class="action-date">（月〜金  10:00 - 19:00）</span>
										</p>
									</div>
									<div class="inquiry-block-action-col2">
										<a href="#"><span class="glyph glyph-mail-white"></span>メールでお問い合わせ</a>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="site-header-menu-items apply">
						<a href="#" class="anc anc-list-opener">
							<span class="glyph glyph-apply-white"></span><br/>
							<span class="text">APPLY</span>
						</a>
						<ul class="site-submenu site-header-submenu main-site-header-submenu lvl2 display-none">
							<li>
								<div class="wish-to-enroll-block">
									<div class="wish-to-enroll-blocks wish-to-enroll-block-2">
										<div class="wish-to-enroll-action wish-to-enroll-action-col1">
											<a href="#"><span class="glyph glyph-apply-yellow"></span>入学を申し込む</a>
										</div>
										<div class="wish-to-enroll-action wish-to-enroll-action-col2">
											<a href="#"><span class="glyph glyph-consoltation-white"></span>無料面談</a>
											<a href="#" class="item-last"><span class="glyph glyph-catalog-white"></span>資料請求</a>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</div>
							</li>
						</ul>
						
					</li>
					<div class="clear-both"></div>
				</ul>
			</nav>
		</header>
		
		
		
		<div id="site-main">
			<div class="main-shoutout-wrapper">
				<div class="shoutout main-shoutout">
					<div class="shoutout-branding">
						<header>
							<h4>
								フィリピン留学
							</h4>
						</header>
						<div class="banner-wrapper">
							<div class="banner">
								<div class="banner-col banner-col1">
									<p>
										社会人シェア<br/>
										“圧倒的”
									</p>
								</div>
								<div class="banner-col banner-col2">
									<p>
										No.1
									</p>
								</div>
								<div class="clear-both"></div>
							</div>
						</div>
					</div>
					<div class="shoutout-contents">
						<header>
							<h1>
								オトナの英語留学
							</h1>
						</header>
						<p>
							社会人の留学は 誰と 学ぶかで全てが変わる
						</p>
					</div>
					<div class="scroll-down shoutout-scroll-down">
						<p>
							<a href="#">
								SCROLL
								<br/>
								<span class="glyph glyph-arrow-down-white"></span>
							</a>
						</p>
					</div>
				</div>
			</div>
			
			<div class="featured-block main-featured-block">
				
				
				
				<header>
					<h2>
						フィリピン留学で唯一の、<br/>
						社会人のための語学学校です。
					</h2>
					<p>
						グローバル化が進んでいる今、社会人こそ語学習得が必要に迫られています。
					</p>
					<p>
						ビジネス現場で通用する英語を習得したい。いつか海外で活躍したい。
					</p>
					<p>					
						”オトナ留学MBA”では同じ目標をもつ様々な社会人との出会いがあります。
					</p>
				</header>
				
				<!-- 
				<div style="width: 60%; margin: 0px auto; position: absolute; background: wheat none repeat scroll 0% 0%; left: 20%; top: 36%;">
					<svg x="0px" y="0px" width="100%" height="400px">
						<line fill="none" stroke="#000" x1="0" y1="0" x2="100%" y2="100%"  stroke-dasharray="10, 10" stroke-width="6" />
					</svg>
				</div>
				-->

				<ul>
					<li class="featured-feature odd">
						<div class="featured-feature-blocks feature-image-wrapper">
							<img src="images/feature-img1.png" />
						</div>
						<div class="featured-feature-blocks feature-vspacing">
						</div>
						<div class="featured-feature-blocks feature-details">
							<header>
								<h5><span class="glyph glyph-feature"></span>MBAの特長</h5>
							</header>
							<p>他校とは全く違う！オトナ達の社交場</p>
							<a href="#"><span class="glyph glyph-arrow-right-white"></span>MBAの特長を見る</a>
						</div>
						<div class="clear-both"></div>
					</li>
					<li class="featured-feature even">
						<div class="featured-feature-blocks feature-image-wrapper">
							<img src="images/feature-img2.png" />
						</div>
						<div class="featured-feature-blocks feature-vspacing">
						</div>
						<div class="featured-feature-blocks feature-details">
							<header>
								<h5><span class="glyph glyph-course"></span>選べる学習スタイル</h5>
							</header>
							<p>あなたに合わせたコースをご用意</p>
							<a href="#"><span class="glyph glyph-arrow-right-white"></span>コース紹介を見る</a>
						</div>
						<div class="clear-both"></div>
					</li>
					<li class="featured-feature odd item-last">
						<div class="featured-feature-blocks feature-image-wrapper">
							<img src="images/feature-img3.png" />
						</div>
						<div class="featured-feature-blocks feature-vspacing">
						</div>
						<div class="featured-feature-blocks feature-details">
							<header>
								<h5><span class="glyph glyph-schedule"></span>充実した生活環境</h5>
							</header>
							<p>学習環境だけでなく毎日の生活も充実</p>
							<a href="#"><span class="glyph glyph-arrow-right-white"></span>スケジュールを見る</a>
						</div>
						<div class="clear-both"></div>
					</li>
					<div class="clear-both"></div>
				</ul>
				<div class="clear-both"></div>
			</div>
			
			<div class="voice-of-graduates-wrapper">
				<div class="voice-of-graduates">
					<header>
						<h2>VOICE <span class="font-baskerville">of</span> GRADUATES</h2>
					</header>
					<p>
						どのような経験をして、</br>どのように生活が変化するのか。</br>
						実際に人生の分岐点を体験した<br/>
						卒業生の声をご紹介します。
					</p>
					<a href="#">卒業生の声 〜Cross Story〜</a>
				</div>
			</div>
			
			<div class="updates-block-wrapper">
				<div class="updates-block">
					<div class="news-block">
						<div class="news-information-block">
							<div class="news-information-blocks news-information-block-1">
								<h4>INFORMATION</h4>
								<p>お知らせ</p>
							</div>
							<div class="news-information-blocks news-information-block-2">
								<ul>
									<li class="news-information-item">
										<article>
											<div class="news-information-item-article-detail news-information-item-date">
												<p>2015.11.24 <a href="#" class="article-link link-blog">BLOG</a></p>
											</div>
											<div class="news-information-item-article-detail news-information-item-headline">
												<p>【歳の数だけテキーラに挑戦！】AJITO店長「男・水田輝行」生誕祭！！</p>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<li class="news-information-item">
										<article>
											<div class="news-information-item-article-detail news-information-item-date">
												<p>2015.11.10 <a href="#" class="article-link link-blog">BLOG</a></p>
											</div>
											<div class="news-information-item-article-detail news-information-item-headline">
												<p>フィリピンのハロウィンパーティーが気合入りすぎな件</p>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<li class="news-information-item item-last">
										<article>
											<div class="news-information-item-article-detail news-information-item-date">
												<p>2015.10.08 <a href="#" class="article-link link-news">NEWS</a></p>
											</div>
											<div class="news-information-item-article-detail news-information-item-headline">
												<p>11月キャンペーン【なんと、留学が１週間無料！！】</p>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<div class="clear-both"></div>
								</ul>
							</div>
							<a class="more-article-link" href="#">MORE</a>
							<div class="clear-both"></div>
						</div>
						<div class="news-media-block">
							<div class="news-media-blocks news-media-block-1">
								<h4>MEDIA RELEASE</h4>
								<p>メディア掲載</p>
							</div>
							<div class="news-media-blocks news-media-block-2">
								<ul>
									<li class="news-media-item">
										<article>
											<div class="news-media-item-article-blocks news-media-item-article-block-1">
												<a href="#">
													<img src="images/media-pic-1.gif" />
												</a>
											</div>
											<div class="news-media-item-article-blocks feature-vspacing">
											</div>
											<div class="news-media-item-article-blocks news-media-item-article-block-2">
												<header>
													<h6>2015.11.28</h6>
												</header>
												<p>
													<a href="#">
														Yahoo!Japanにて本校が紹介されました。
													</a>
												</p>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<li class="news-media-item item-last">
										<article>
											<div class="news-media-item-article-blocks news-media-item-article-block-1">
												<a href="#">
													<img src="images/media-pic-2.gif" />
												</a>
											</div>
											<div class="news-media-item-article-blocks feature-vspacing">
											</div>
											<div class="news-media-item-article-blocks news-media-item-article-block-2">
												<header>
													<h6>2015.11.24</h6>
												</header>
												<p>
													<a href="#">
														TBSテレビのNews23にて本校が紹介されました。
													</a>
												</p>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<div class="clear-both"></div>
								</ul>
							</div>
							<a class="more-media-link" href="#"><span class="glyph glyph-arrow-right-white"></span>MORE</a>
							<div class="clear-both"></div>
						</div>
					</div>
					<div class="campaign-block">
						<header>
							<h3>CAMPAIGN</h3>
						</header>
						<article>
							<a href="#">
								<img src="images/campaign-pic-1.jpg" />
							</a>
						</article>
						<article>
							<a href="#">
								<img src="images/campaign-pic-2.jpg" />
							</a>
						</article>
					</div>					
				</div>
				
				<div class="carousel-block gallery-carousel-block">
					<div class="gallery-carousel-block-slick">
						<div>
							<img src="images/galleryr-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/galleryr-slide-img-2.jpg" />
						</div>
						<div>
							<img src="images/galleryr-slide-img-3.jpg" />
						</div>
						<div>
							<img src="images/galleryr-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/galleryr-slide-img-2.jpg" />
						</div>
						<div>
							<img src="images/galleryr-slide-img-3.jpg" />
						</div>
					</div>
				</div>
				
			</div>			
		</div>
		
		
		
		<footer id="site-footer">
			<div class="wish-to-enroll-block">
				<div class="wish-to-enroll-blocks wish-to-enroll-block-1">
					<header>
						<h4><span class="glyph glyph-apply-blue"></span>入学をご希望の方</h4>
					</header>
				</div>
				<div class="wish-to-enroll-blocks wish-to-enroll-block-2">
					<div class="wish-to-enroll-action wish-to-enroll-action-col1">
						<a href="#"><span class="glyph glyph-apply-white"></span>入学を申し込む</a>
					</div>
					<div class="wish-to-enroll-action wish-to-enroll-action-col2">
						<a href="#">無料面談</a>
						<a href="#" class="item-last">資料請求</a>
						<div class="clear-both"></div>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
			
			<div class="inquiry-block">
				<div class="inquiry-blocks inquiry-block-1">
					<header>
						<h4><span class="glyph glyph-contacts-blue"></span>お問い合わせ</h4>
					</header>
				</div>
				<div class="inquiry-blocks inquiry-block-2">
					<div class="inquiry-block-action-col1">
						<p>
							<span class="glyph glyph-phone-blue"></span>03-4405-8478<br/>
							<span class="action-date">（月〜金  10:00 - 19:00）</span>
						</p>
					</div>
					<div class="inquiry-block-action-col2">
						<a href="#"><span class="glyph glyph-mail-white"></span>メールでお問い合わせ</a>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
			
			<div class="site-footer-ads-block-wrapper">
				<div class="ads-block site-footer-ads-block">
					<img src="images/ad-pic-1.jpg" />
					<img src="images/ad-pic-2.jpg" class="item-last" />
				</div>
			</div>
			
			<div class="site-footer-carousel-block-wrapper">
				<div class="carousel-block site-footer-carousel-block">
					<div class="site-footer-carousel-block-slick">
						<div>
							<img src="images/footer-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-2.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-2.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-2.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-2.jpg" />
						</div>
					</div>
				</div>
			</div>
			
			<div class="site-footer-main-block-wrapper">
				<div class="site-footer-main-block">
					<nav class="site-menu site-footer-menu social-site-footer-menu">
						<ul>
							<li>
								<a href="#"><span class="glyph glyph-facebook-circle-gray"></span></a>
							</li>
							<li>
								<a href="#"><span class="glyph glyph-twitter-circle-gray"></span></a>
							</li>
							<li>
								<a href="#"><span class="glyph glyph-instagram-circle-gray"></span></a>
							</li>
							<li>
								<a href="#"><span class="glyph glyph-line-circle-gray"></span></a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<nav class="site-menu site-footer-menu main-site-footer-menu">
						<ul>
							<li>
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>会社概要</a>
							</li>
							<li>
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>キャンセル規定</a>
							</li>
							<li class="item-last">
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>免責事項</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<p>
						Copyright © United Re-Growth Co.Ltd All rights reserved.
					</p>
				</div>
			</div>
			
		</footer>
		
	</body>
</html>