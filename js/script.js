$(document).ready(function(){
	/* slick slider start */
	$('.site-footer-carousel-block-slick').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		arrows:false,
		dots:true,
		variableWidth: true
	});
	$('.gallery-carousel-block-slick').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 2,
		variableWidth: true
	});
	/* slick slider end */

	$('.anc-list-opener').click(function(event){
		//check if this ! have class open
		if( !$(this).hasClass('open') )
		{
			//check if parent ! have an open class
			if( !$(this).parents('.site-submenu.site-header-submenu.main-site-header-submenu.open').length )
			{
				//close other opened menu first
				$('.anc-list-opener.open').next('ul').toggleClass("display-none open");
				$('.anc-list-opener.open').toggleClass("open");
			}
		}
		//open menu if closed, close menu if opened
		$(this).toggleClass("open");
		$(this).next('ul').toggleClass("display-none open");
		//
		event.preventDefault();
		event.stopPropagation(); //important, so $(document).click don't propagate after this
	});
	
	$(document).click(function(e) {
		//close a menu when clicking outside it, if there is an open menu
		//check if click ! have class of anc-list-opener
		if( !$(e.target).hasClass('anc-list-opener') )
		{
			if( $('.site-menu.site-header-menu.main-site-header-menu').find('.site-submenu.site-header-submenu.main-site-header-submenu.open').length )
			{
				$('.anc-list-opener.open').next('ul').toggleClass("display-none open");
				$('.anc-list-opener.open').toggleClass("open");
			}	
		}
	});
});